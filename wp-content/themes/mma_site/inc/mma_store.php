<div class="anuncio conheca-store">
	<div class="large-container">
		<div class="anuncio-background anuncio-background-store" style="background-image: url('<?php echo $configuracao['opt_mma_store_foto_home']['url'] ?>');">
			<div class="row">
				<div class="col-sm-7">
					<h2 class="titulo"><?php echo $configuracao['opt_mma_store_titulo_home'] ?></h2>
				</div>
				<div class="col-sm-5">
					<div class="div-button-padrao">
						<a href="#" class="button-padrao">Visitar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>