<?php

/**
 * Template Name: Home
 * Description: Página Home
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MMA
 */
$destaques   = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 1) );
$servicos    = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 10) );
$projetos    = new WP_Query( array( 'post_type' => 'projeto', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 10) );
$eventos     = new WP_Query( array( 'post_type' => 'evento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );

$numero_whtas_remocao_espaco_externo = trim($configuracao['opt_telefone']);
$numero_whtas_remocao_traco = str_replace(" ","",$numero_whtas_remocao_espaco_externo);
$numeroFomratado = str_replace("-","",$numero_whtas_remocao_traco);

get_header(); ?>
<main class="pg pg-inicial">
	
	<section class="secao-destaque">
		<h4 class="hidden">SEÇÃO DESTAQUE</h4>
		<div class="carrossel-destaque owl-carousel">
			<?php if ($destaques): while ( $destaques->have_posts() ) : $destaques->the_post(); ?>
			<div class="item-destaque" style="background-image:url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>)">
				<div class="large-container">
					<article>
						<h1 class="titulo"><?php echo get_the_title() ?></h1>
						<p><?php echo get_the_content() ?></p>
						<div class="div-button-padrao">
							<span class="button-padrao">Agendar Horário</span>
						</div>
					</article>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); endif; ?>
		</div>
	</section>

	<section class="secao-servicos">
		<h4 class="hidden"><?php echo $configuracao['opt_servico_titulo_home'] ?></h4>
		<div class="servicos-container">
			<div class="row">
				<div class="col-md-5">
					<article>
						<h2 class="titulo"><?php echo $configuracao['opt_servico_titulo_home'] ?></h2>
						<p><?php echo $configuracao['opt_servico_texto_home'] ?></p>

						<?php  if (trim($configuracao['opt_servico_link_home'])): ?>
						<div class="div-button-padrao">
							<a href="<?php echo $configuracao['opt_servico_link_home'] ?>" class="button-padrao button-padrao-preto">Ver todos</a>
						</div>
					<?php endif; ?>
					</article>
				</div>
				<div class="col-md-7">
					<?php if ($servicos):  ?>
					<ul class="carrossel-servicos owl-carousel">
						
						<?php while ( $servicos->have_posts() ) : $servicos->the_post(); ?>
						<li class="item-servico" style="background-image: url('<?php echo rwmb_meta('MMA_servico_img_home')['full_url'] ?>');">
							<figure class="hidden">
								<img src="<?php echo rwmb_meta('MMA_servico_img_home')['full_url'] ?>" alt="Imagem serviço">
								<figcaption><?php echo get_the_title() ?></figcaption>
							</figure>
							<div class="conteudo-servico">
								<h3 class="titulo"><?php echo get_the_title() ?></h3>
								<p><?php echo the_excerpt(); ?></p>
							</div>
						</li>
						<?php endwhile; wp_reset_query(); ?>

					</ul>
					<?php  endif; ?>
				</div>
			</div>
		</div>
	</section>
	
	<?php include (TEMPLATEPATH . '/inc/mma_agendamento.php'); ?>

	<?php if ($projetos): ?>
	<section class="secao-sobre-projeto">
		<h4 class="hidden">SEÇÃO PROJETO</h4>
		<div class="large-container">
			<div class="carrossel-projeto owl-carousel">
				
				<?php  
					while ( $projetos->have_posts() ) : $projetos->the_post();
						$imagem_projeto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];
						$link_projeto = get_permalink();
						$single_projeto_servico = rwmb_meta('MMA_single_projeto_servico');
				?>
				<div class="item-projeto">
					<div class="mid-container">
						<article>
							<h2 class="titulo"><?php echo get_the_title(); ?></h2>
							<div class="sobre-projeto">
								<h4 class="subtitulo">Sobre o projeto</h4>
								<p><?php echo get_the_content(); ?></p>
								<div class="div-button-padrao">
									<a href="<?php echo get_permalink(); ?>" class="button-padrao button-padrao-preto">Ver mais</a>
								</div>
							</div>
							<ul>
								<?php 
								$contador = 0;
								//LOOP DE POST PROJETOS
								$loop_servicos_projeto = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'posts_per_page' => -1) );
								while ( $loop_servicos_projeto->have_posts() ) : $loop_servicos_projeto->the_post();

									$terms = get_the_terms( $post->ID, 'categoriaservicos' );
									foreach ($terms as $term) {
										$term_link = get_term_link($term->slug, 'categoriaservicos');
									}

									$verificacao =  in_array($post->ID, $single_projeto_servico);

									if ($verificacao):
								?>
								<li><a href="<?php echo $term_link; ?>" target="_blank"><?php echo get_the_title(); ?></a></li>
								<?php $contador++; endif; endwhile; wp_reset_query(); ?>
							</ul>
							<div class="div-button-padrao">
								<a href="<?php echo $link_projeto; ?>" class="button-padrao button-padrao-preto">Ver mais</a>
							</div>
						</article>
					</div>
					<a href="<?php echo $link_projeto; ?>">
						<figure>
							<img src="<?php echo $imagem_projeto; ?>" alt="Imagem projeto">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
						</figure>
					</a>
				</div>
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php include (TEMPLATEPATH . '/inc/mma_store.php'); ?>

	<?php if ($eventos):?>
	<section class="secao-eventos">
		<h4 class="hidden">SEÇÃO EVENTOS</h4>
		<div class="large-container">
			<div class="row">
				<div class="col-sm-6">
					<h2 class="titulo">Eventos</h2>
				</div>
				<div class="col-sm-6">
					<div class="div-button-padrao">
						<a href="<?php echo esc_url( home_url( '/eventos/' ) ); ?>" class="button-padrao button-padrao-preto">Ver todos</a>
					</div>
				</div>
			</div>
			<ul class="lista-eventos">
				
				<?php $i = 1; while ( $eventos->have_posts() ) : $eventos->the_post(); ?>
				<li class="<?php if($i == 2){echo "evento-centro";} ?>">
					<a href="<?php echo get_permalink(); ?>" class="link-imagem">
						<figure>
							<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</a>
					<span class="data-evento"><?php echo get_the_date( 'j M Y' ); ?></span>
					<a href="<?php echo get_permalink(); ?>" class="link-titulo">
						<h2 class="titulo titulo-evento"><?php echo get_the_title() ?></h2>
					</a>
				</li>
				<?php $i++; endwhile; wp_reset_query(); ?>

			</ul>
			<div class="div-button-padrao">
				<a href="<?php echo esc_url( home_url( '/eventos/' ) ); ?>" class="button-padrao button-padrao-preto">Ver todos</a>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php  
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');

		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>

</main>
<?php get_footer();