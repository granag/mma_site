<section class="secao-acessoria" id="acessoria">
	<h4 class="hidden"><?php echo $configuracao['opt_mma_news_titulo_home'] ?></h4>
	<div class="large-container">
		<div class="row">
			<div class="col-md-6">
				<article>
					<h2 class="titulo"><?php echo $configuracao['opt_mma_news_titulo_home'] ?></h2>
					<p><?php echo $configuracao['opt_mma_news_texto_home'] ?></p>
					<?php echo do_shortcode('[contact-form-7 id="61" title="Contato ACESSORIA DA MMA"]'); ?>
				</article>
			</div>
			<div class="col-md-6">
				<figure>
					<img src="<?php echo $configuracao['opt_mma_news_foto_home']['url'] ?>" alt="<?php echo $configuracao['opt_mma_news_titulo_home'] ?>" class="hidden">
					<figcaption class="hidden"><?php echo $configuracao['opt_mma_news_titulo_home'] ?></figcaption>
				</figure>
			</div>
		</div>
	</div>
</section>