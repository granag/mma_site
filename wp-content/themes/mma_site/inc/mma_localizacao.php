<?php $enderecos   = new WP_Query( array( 'post_type' => 'endereco', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) ); ?>

<?php if ($enderecos): ?>
<section class="secao-localizacao" id="secao-localizacao">
	<h4 class="hidden">SEÇÃO LOCALIZAÇÃO</h4>
		<ul>
			<?php while ( $enderecos->have_posts() ) : $enderecos->the_post(); ?>
<?php //var_dump(rwmb_meta('MMA_endereco_mapa')); ?>
<?php //var_dump(rwmb_meta('MMA_endereco_mapa', array('size' => 'full'))['full_url']); ?>
			<li>
				<article class="localizacao">
					<h2 class="titulo"><?php echo get_the_title(); ?></h2>
					<figure style="background-image: url(<?php echo rwmb_meta('MMA_endereco_mapa', array('size' => 'full'))['full_url']; ?>);">
						<a href="<?php echo rwmb_meta('MMA_endereco_link') ?>" target="_blank">
							<img src="<?php echo rwmb_meta('MMA_endereco_mapa', array('size' => 'full'))['full_url']; ?>" alt="<?php echo get_the_title(); ?>" class="hidden">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
						</a>
					</figure>
					<ul class="sobre-localizacao">
						<li><?php echo rwmb_meta('MMA_endereco_rua') ?></li>
						<li><?php echo rwmb_meta('MMA_endereco_telefone') ?></li>
						<li><?php echo rwmb_meta('MMA_endereco_email') ?></li>
					</ul>
				</article>
				<figure style="background-image: url(<?php echo rwmb_meta('MMA_endereco_mapa', array('size' => 'full'))['full_url']; ?>);">
					<a href="<?php echo rwmb_meta('MMA_endereco_link') ?>" target="_blank">
						<img src="<?php echo rwmb_meta('MMA_endereco_mapa', array('size' => 'full'))['full_url']; ?>" alt="<?php echo get_the_title(); ?>" class="hidden">
						<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
					</a>
				</figure>
			</li>
			<?php endwhile; ?>
		</ul>
</section>
<?php  endif; ?>