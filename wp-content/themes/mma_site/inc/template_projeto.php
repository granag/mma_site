<li>
	<div class="row">
		<div class="col-4 col-imagem">
			<a href="<?php echo get_permalink() ?> " class="link-imagem-projeto">
				<figure style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>)">
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
				</figure>
				<div class="nome-projeto">
					<h2 class="titulo"><?php echo get_the_title() ?></h2>
					<img src="<?php echo get_template_directory_uri(); ?>/img/plus_square@1,5x.svg" alt="Ícone mais">
				</div>
			</a>
		</div>
		<div class="col-8 col-conteudo">
			<div class="sobre-projeto">
				<div class="nome-projeto">
					<a href="<?php echo get_permalink() ?> ">
						<h2 class="titulo"><?php echo get_the_title() ?></h2>
						<img src="<?php echo get_template_directory_uri(); ?>/img/plus_square@1,5x.svg" alt="Ícone mais">
					</a>
				</div>
				<ul class="info-projeto">
					<?php 
						$single_projeto_servico = rwmb_meta('MMA_single_projeto_servico');
						$contador = 0;
						//LOOP DE POST PROJETOS
						$loop_servicos_projeto = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'posts_per_page' => -1) );
						while ( $loop_servicos_projeto->have_posts() ) : $loop_servicos_projeto->the_post();

							$terms = get_the_terms( $post->ID, 'categoriaservicos' );
							foreach ($terms as $term) {
								$term_link = get_term_link($term->slug, 'categoriaservicos');
							}

							$verificacao =  in_array($post->ID, $single_projeto_servico);

							if ($verificacao && $contador < 3):
						?>
						<li><a href="<?php echo $term_link; ?>" target="_blank"><?php echo get_the_title(); ?></a></li>
					<?php $contador++; endif; endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</div>
	</div>
</li>