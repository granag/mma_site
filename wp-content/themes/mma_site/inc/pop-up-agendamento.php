<div class="pop-up-agendamento">
	<div class="pop-up-lente"></div>
	<div class="pop-up-container">
		<div class="pop-up">
			<div class="row">
				<div class="col-md-7">
					<article>
						<span class="close-pop-up"><img src="<?php echo get_template_directory_uri(); ?>/img/close_contact@1,5x.svg" alt="Fechar pop-up"></span>
						<h2 class="titulo">Agende <br>seu horário</h2>
						<ul class="lista-enderecos">
							<li><span>Alto da xv</span> 442 Dickinson Mews</li>
							<li><span>Ecoville</span> 483 Adalberto Course Suite 666</li>
							<li><span>Batel</span> 19 Wunsch Plains Apt. 797</li>
							<li><span>Balneário</span> 0313 Wilderman Ways Suite 181</li>
							<li><span>Alphaville</span> 064 Kuphal Land Suite 317</li>
						</ul>
						<ul class="lista-contatos">
							<li>41 99131-0225</li>
							<li>contato@mma.com.br</li>
						</ul>

						<div class="div-button-padrao">
							<span class="button-padrao button-padrao-preto">Enviar E-mail</span>
						</div>
					</article>
				</div>
				<div class="col-md-5">
					<div class="formulario-contato">
						<span class="close-sub-menu show-close-sub-menu"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow_servicos_right_white@1,5x.svg" alt="Fechar formulário mobile"></span>
						<span class="close-pop-up"><img src="<?php echo get_template_directory_uri(); ?>/img/close_contact@1,5x.svg" alt="Fechar pop-up"></span>
						<h2 class="titulo">Agende <br>seu horário</h2>
						<?php echo do_shortcode('[contact-form-7 id="248" title="Formulário de contato pop-up"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>