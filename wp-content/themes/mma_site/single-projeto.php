<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MMA
 */

get_header();


	while ( have_posts() ) :
		the_post();
		$projeto_galeria_carrossel   = rwmb_meta('MMA_projeto_galeria');
		$projeto_galeria_carrossel_2 = rwmb_meta('MMA_projeto_galeria_2');
		$projeto_galeria_carrossel_3 = rwmb_meta('MMA_projeto_galeria_3');
		$single_projeto_servico = rwmb_meta('MMA_single_projeto_servico');

?>
<main class="pg pg-projeto">
	<section class="secao-destaque-single" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>)">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<figure class="hidden">
			<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>">
			<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
		</figure>
	</section>

	<section class="secao-sobre-projeto">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<div class="mid-container">
			<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="voltar"><img src="<?= get_template_directory_uri(); ?>/img/arrow_servicos_left_black@1,5x.svg" alt="Seta voltar"></a>
			<article>
				<h2 class="titulo"><?php echo get_the_title(); ?></h2>
				<div class="sobre-projeto">
					<h4 class="subtitulo">Sobre o projeto</h4>
					<p><?php echo get_the_content() ; ?></p>
				</div>
				<ul>
					 <?php 
					 	$contador = 0;
						//LOOP DE POST PROJETOS
						$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'posts_per_page' => -1) );
						while ( $servicos->have_posts() ) : $servicos->the_post();

							$terms = get_the_terms( $post->ID, 'categoriaservicos' );
							foreach ($terms as $term) {
								$term_link = get_term_link($term->slug, 'categoriaservicos');
							}

							$verificacao =  in_array($post->ID, $single_projeto_servico);

							if ($verificacao):
					?>
					<li><a href="<?php echo $term_link; ?>" target="_blank"><?php echo get_the_title(); ?></a></li>
					<?php $contador++; endif; endwhile; wp_reset_query(); ?>
				</ul>
			</article>
		</div>
	</section>

	<section class="secao-galeria-projeto">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<div class="large-container">
			<ul class="lista-fotos">
				<?php 
					$cont = 1;
					$projeto_galeria = rwmb_meta('MMA_projeto_galeria');
					var_dump($projeto_galeria);
					foreach ($projeto_galeria as $projeto_galeria):

						if($cont == 2){
							$classe = "evento-centro";
						}else if($cont == 5){
							$classe = "large-image large-image-right";
						}else{
							$classe = "evento";
						}
				?>
				<li class="<?php echo $classe; ?>">
					<figure>
						<img src="<?php echo $projeto_galeria['full_url'] ?>" alt="<?php echo $projeto_galeria['full_url'] ?>">
						<figcaption class="hidden"><?php echo $projeto_galeria['full_url'] ?></figcaption>
					</figure>
				</li>
				<?php $cont++; endforeach; ?>
			</ul>

			<ul class="lista-fotos">
				<?php 
					$cont = 1;
					$projeto_galeria_2 = rwmb_meta('MMA_projeto_galeria_2');
					foreach ($projeto_galeria_2 as $projeto_galeria_2):

						if($cont == 2){
							$classe = "evento-centro";
						}else if($cont == 4){
							$classe = "large-image large-image-left";
						}else{
							$classe = "evento";
						}
				?>
				<li class="<?php echo $classe; ?>">
					<figure>
						<img src="<?php echo $projeto_galeria_2['full_url'] ?>" alt="<?php echo $projeto_galeria_2['full_url'] ?>">
						<figcaption class="hidden"><?php echo $projeto_galeria_2['full_url'] ?></figcaption>
					</figure>
				</li>
				<?php $cont++; endforeach; ?>
			</ul>

			<ul class="lista-fotos">
				<?php 
					$cont = 1;
					$projeto_galeria_3 = rwmb_meta('MMA_projeto_galeria_3');
					foreach ($projeto_galeria_3 as $projeto_galeria_3):

						if($cont == 2){
							$classe = "evento-centro";
						}else if($cont == 5){
							$classe = "large-image large-image-right";
						}else{
							$classe = "evento";
						}
				?>
				<li class="<?php echo $classe; ?>">
					<figure>
						<img src="<?php echo $projeto_galeria_3['full_url'] ?>" alt="<?php echo $projeto_galeria_3['full_url'] ?>">
						<figcaption class="hidden"><?php echo $projeto_galeria_3['full_url'] ?></figcaption>
					</figure>
				</li>
				<?php $cont++; endforeach; ?>
			</ul>
			
		</div>
	</section>

	<!-- SE HOUVER POST POSTERIOR MOSTRA A FOTO SE NÃO MOSTRA A FOTO DO PRIMEIRO POST DISPONÍVEL -->
	<?php 

		$nextPost = get_next_post(); 
		$nextFoto = wp_get_attachment_image_src( get_post_thumbnail_id($nextPost->ID), 'full' );
		$nextFoto = $nextFoto[0];

		$previousPost = get_previous_post();
		$previousPostFoto = wp_get_attachment_image_src( get_post_thumbnail_id($previousPost->ID), 'full' );
		$previousPostFoto = $previousPostFoto[0];
		$previousPost->post_title;
	?>
	<div class="proximos-projetos">
		<ul class="paginador">
			
			<li class="arrow-left">
				<?php if ($nextPost):?>
				<a href="<?php echo $nextPost->guid ?>">
					<span class="seta-paginador">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow_servicos_left_black@1,5x.svg" alt="Seta projeto anterior" class="black-arrow">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow_left_white@1,5x.svg" alt="Seta projeto anterior" class="white-arrow">
					</span>
					<figure>
						<img src="<?php echo $nextFoto ?>" alt="<?php echo $nextPost->post_title ?>">
						<figcaption class="hidden"><?php echo $nextPost->post_title ?></figcaption>
					</figure>
					<div class="nome-projeto">
						<h2 class="titulo"><?php echo $nextPost->post_title ?></h2>
					</div>
				</a>
				<?php endif;  ?>
			</li>
		
			<li class="arrow-right">
				<?php  if ($previousPost): ?>
				<a href="<?php echo $previousPost->guid ?>">
					<div class="nome-projeto">
						<h2 class="titulo"><?php echo $previousPost->post_title ?></h2>
					</div>
					<figure>
						<img src="<?php echo $previousPostFoto ?>" alt="<?php echo $previousPost->post_title ?>">
						<figcaption class="hidden"><?php echo $previousPost->post_title ?></figcaption>
					</figure>
					<span class="seta-paginador">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow_servicos_right_black@1,5x.svg" alt="Seta projeto anterior" class="black-arrow">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow_right_white@1,5x.svg" alt="Seta projeto anterior" class="white-arrow">
					</span>
					<?php endif;?>
				</a>
			</li>
			
		</ul>
	</div>



	
	<?php  
		include (TEMPLATEPATH . '/inc/galeria.php');
		include (TEMPLATEPATH . '/inc/mma_store.php');
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');
		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>
	


	<div class="pop-up-galeria-projeto">
		<span>
			<img src="<?php echo get_template_directory_uri(); ?>/img/close_contact@1,5x.svg" alt="Fechar pop-up">
		</span>
		<ul class="carrossel-projetos-galeria owl-carousel">
			<?php foreach ($projeto_galeria_carrossel as $projeto_galeria_carrossel): ?>
				<li class="item-projetos">
					<figure>
						<img src="<?php echo $projeto_galeria_carrossel['full_url'] ?>" alt="<?php echo $projeto_galeria_carrossel['full_url'] ?>">
						<figcaption><?php echo $projeto_galeria_carrossel['full_url'] ?></figcaption>
					</figure>
				</li>
			<?php endforeach;  ?>

			<?php foreach ($projeto_galeria_carrossel_2 as $projeto_galeria_carrossel_2): ?>
				<li class="item-projetos">
					<figure>
						<img src="<?php echo $projeto_galeria_carrossel_2['full_url'] ?>" alt="<?php echo $projeto_galeria_carrossel_2['full_url'] ?>">
						<figcaption><?php echo $projeto_galeria_carrossel_2['full_url'] ?></figcaption>
					</figure>
				</li>
			<?php endforeach;  ?>

			<?php foreach ($projeto_galeria_carrossel_3 as $projeto_galeria_carrossel_3): ?>
				<li class="item-projetos">
					<figure>
						<img src="<?php echo $projeto_galeria_carrossel_3['full_url'] ?>" alt="<?php echo $projeto_galeria_carrossel_3['full_url'] ?>">
						<figcaption><?php echo $projeto_galeria_carrossel_3['full_url'] ?></figcaption>
					</figure>
				</li>
			<?php endforeach;  ?>
		</ul>
	</div>

</main>

<?php endwhile; wp_reset_query(); get_footer();
