<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MMA
 */
global $configuracao;
$numero_whtas_remocao_espaco_externo = trim($configuracao['opt_telefone']);
$numero_whtas_remocao_traco = str_replace(" ","",$numero_whtas_remocao_espaco_externo);
$numeroFomratado = str_replace("-","",$numero_whtas_remocao_traco);

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Michroma|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
		<div class="large-container">
			<div class="row">
				<!-- LOGO -->
				<div class="col-lg-2">
					<?php if (trim($configuracao['opt_logo']['url'])): ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
						<figure>
							<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php bloginfo( 'name' ); ?>">
							<figcaption class="hidden"><?php bloginfo( 'name' ); ?></figcaption>
						</figure>
					</a>
					<?php endif; ?>
				</div>
				<!-- MENU -->	
				<div class="col-lg-8">
					<nav class="navbar navbar-expand-lg">
						<button class="open-menu-button" type="button">
							<span class="open-menu-button-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/menu.svg" alt="Ícone menu"></span>
						</button>
						<div class="navbar-header">
							<?php if (trim($configuracao['opt_logo_branca']['url'])): ?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo logo-branca">
									<figure>
										<img class="img-responsive" src="<?php echo $configuracao['opt_logo_branca']['url'] ?>" alt="<?php bloginfo( 'name' ); ?>">
										<figcaption class="hidden"><?php bloginfo( 'name' ); ?></figcaption>
									</figure>
								</a>
							<?php endif; ?>
							<span class="close-navbar-header"><img src="<?php echo get_template_directory_uri(); ?>/img/close_contact@1,5x.svg" alt="Fechar menu"></span>
							<span class="close-sub-menu"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow_servicos_right_white@1,5x.svg" alt="Fechar submenu"></span>
							<?php wp_nav_menu(); ?>
							<ul class="redes-sociais">
								<?php if (trim($configuracao['opt_redes_sociais_instagram'])): ?>
								<li>
									<a href="<?php echo $configuracao['opt_redes_sociais_instagram'] ?> " target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/img/Instagram_white.svg" alt="Instagram">
									</a>
								</li>
								<?php endif; if (trim($configuracao['opt_redes_sociais_twitter'])): ?>
								<li>
									<a href="<?php echo $configuracao['opt_redes_sociais_twitter'] ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/img/Twitter_white.svg" alt="Twitter">
									</a>
								</li>
								<?php endif; if (trim($configuracao['opt_redes_sociais_facebook'])): ?>
								<li>
									<a href="<?php echo $configuracao['opt_redes_sociais_facebook'] ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri(); ?>/img/Facebook_white.svg" alt="Facebook">
									</a>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</nav>
				</div>
				<!-- REDES SOCIAIS -->
				<div class="col-lg-2">
					<?php if (trim($configuracao['opt_telefone'])): ?>
					<p class="whatsapp">
						<a href="https://api.whatsapp.com/send?phone=+55<?php echo $numeroFomratado; ?>&text=Ol%C3%A1!"><?php echo $configuracao['opt_telefone']; ?></a>
					</p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header>
