<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MMA
 */
global $configuracao;
?>

<footer>
	<div class="large-container">
		<div class="footer">
			<?php if (trim($configuracao['opt_logo']['url'])): ?>
			<figure>
				<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php bloginfo( 'name' ); ?>">
				<figcaption class="hidden"><?php bloginfo( 'name' ); ?></figcaption>
			</figure>
			<?php endif; ?>
			<?php 
				wp_nav_menu( 
					array(
						'menu' => "Header Principal",
						'menu_class' => "menu-footer",
					)
				); 
			?>
			<ul class="redes-sociais">

				<?php if (trim($configuracao['opt_redes_sociais_instagram'])): ?>
				<li>
					<a href="<?php echo $configuracao['opt_redes_sociais_instagram'] ?> " target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/Instagram_1298747@1,5x.svg" alt="Instagram">
					</a>
				</li>
				<?php endif; if (trim($configuracao['opt_redes_sociais_twitter'])): ?>
				<li>
					<a href="<?php echo $configuracao['opt_redes_sociais_twitter'] ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/Twitter_1298770@1,5x.svg" alt="Twitter">
					</a>
				</li>
				<?php endif; if (trim($configuracao['opt_redes_sociais_facebook'])): ?>
				<li>
					<a href="<?php echo $configuracao['opt_redes_sociais_facebook'] ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/Facebook_1298738@1,5x.svg" alt="Facebook">
					</a>
				</li>
				<?php endif; ?>
			</ul>
		</div>

		<?php  if (trim($configuracao['opt_copyright'])): ?>
		<div class="copyright">
			<p><?php echo $configuracao['opt_copyright'] ?></p>
		</div>
		<?php endif; ?>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
