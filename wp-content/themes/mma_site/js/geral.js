(function() {

	$(document).ready(function(){

		$('.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').click(function(){
			event.preventDefault();
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').removeClass("ativo");
			$('.pg-servicos .areaServicos').removeClass("ativo");
			$(this).addClass("ativo");
			let dataId = $(this).attr('data-id');
			let dataIcone = $(this).attr('data-icone');
			let dataTitulo = $(this).attr('data-titulo');

			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu .categoria-ativa a .categoria-ativa-titulo').text(dataTitulo);
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu .categoria-ativa a .categoria-ativa-imagem img').attr('src', dataIcone);
			$("#"+dataId).addClass("ativo");

			if(screen.width <= 768){
				setTimeout(function(){
					$('html,body').animate({
						scrollTop: $('#servico-ancora').offset().top
					}, 1000);
					$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul').toggleClass('show-menu-categorias');
					return false;
				}, 200);
			}
		});

		$('.tax-categoriaservicos .pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').click(function(){
			event.preventDefault();
			let dataLink = $(this).attr('data-link');
			window.history.pushState("object or string", "Title", dataLink);

		});

		$('.carrossel-servicos').owlCarousel({
			items : 3,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false ,
			margin: 15,
			autoWidth: true,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

		$('.carrossel-projetos-galeria').owlCarousel({
			items : 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false ,
			margin: 0,
			autoWidth: false,
		});

		$('.carrossel-projeto').owlCarousel({
			items : 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			animateOut: 'fadeOut',
			center: false,
		});

		$('.carrossel-destaque').owlCarousel({
			items : 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false
		});

		$('.pg-projetos .menu-filtro .filtros .modelo img.grid').click(function(){
			$('.pg-projetos .secao-projetos .lista-projetos').addClass('grid-projetos');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').addClass('col-12');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-12').removeClass('col-4');
			$('.pg-projetos .secao-projetos .grid-projetos li:nth-child(3n+2)').addClass('evento-centro');
			$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
			$(this).addClass('modelo-active');
		});

		$('.pg-projetos .menu-filtro .filtros .modelo img.list').click(function(){
			$('.pg-projetos .secao-projetos .lista-projetos').removeClass('grid-projetos');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-12').addClass('col-4');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').removeClass('col-12');
			$('.pg-projetos .secao-projetos .grid-projetos li').removeClass('evento-centro');
			$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
			$(this).addClass('modelo-active');
		});

		$('.pg .secao-localizacao>ul>li').click(function(e){
			let myThisUl = $(this).children('article').children('ul');
			let myThisFigure = $(this).children('article').children('figure');
			let myThis = $(this);


			if(!$(this).hasClass('localizacao-active')){
				$('.pg .secao-localizacao>ul>li').removeClass('localizacao-active');
				$('.pg .secao-localizacao ul li .localizacao .sobre-localizacao').removeClass('show-sobre');
				$('.pg .secao-localizacao ul li .localizacao figure').removeClass('show-sobre');

				$(this).addClass('localizacao-active');

				setTimeout(function(){
					myThisUl.addClass('show-sobre');
					myThisFigure.addClass('show-sobre');
				}, 200);
			} else{
				myThisUl.removeClass('show-sobre');
				myThisFigure.removeClass('show-sobre');

				setTimeout(function(){
					myThis.removeClass('localizacao-active');
				}, 200);
			}

		});

		$('.pg .secao-acessoria article form .div-input-enviar input').attr('value', '');

		$('.pg-projetos .menu-filtro .filtros .filtro-categoria .filtro-categoria-atual').click(function(){
			$('.pg-projetos .menu-filtro .filtros .filtro-categoria .filtro-categoria-opcoes').toggleClass('show-options');
		});

		$('.contatoMenu a').click(function(e){
			e.preventDefault();
			$('.pop-up-agendamento').addClass('show-pop-up');
			$('body').addClass('stop-scroll');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').addClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento .pop-up-container').addClass('show-pop-up-container');
				}, 400);
			}, 200);
		});

		$('.div-button-padrao span.button-padrao').click(function(){
			$('.pop-up-agendamento').addClass('show-pop-up');
			$('body').addClass('stop-scroll');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').addClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento .pop-up-container').addClass('show-pop-up-container');
				}, 400);
			}, 200);
		});

		$('.pop-up-agendamento .close-pop-up').click(function(){
			$('.pop-up-agendamento .pop-up-container').removeClass('show-pop-up-container');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').removeClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento').removeClass('show-pop-up');
					$('body').removeClass('stop-scroll');
				}, 400);
			}, 200);
		});

		$('.pop-up-agendamento .pop-up-lente').click(function(){
			$('.pop-up-agendamento .pop-up-container').removeClass('show-pop-up-container');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').removeClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento').removeClass('show-pop-up');
					$('body').removeClass('stop-scroll');
				}, 400);
			}, 200);
		});


		$('.pg-projeto .secao-galeria-projeto .lista-fotos li').click(function(){
			$(".pop-up-galeria-projeto").fadeIn();
			$("body").addClass("pop-up-galeria-projeto-sombra");
		});

		$('.pop-up-galeria-projeto>span').click(function(){
			$(".pop-up-galeria-projeto").fadeOut();
			$("body").removeClass("pop-up-galeria-projeto-sombra");
		});

		$('header .navbar .open-menu-button').click(function(){
			$('header .navbar .navbar-header').addClass('show-navbar-header');
			$('header .logo-branca').addClass('show-logo-branca');
		});

		$('header .navbar .navbar-header .close-navbar-header').click(function(){
			$('header .navbar .navbar-header').removeClass('show-navbar-header');
			$('header .logo-branca').removeClass('show-logo-branca');
		});

		$('header .navbar .navbar-header ul li.scrollTop a').click(function(){
			$('header .navbar .navbar-header').removeClass('show-navbar-header');
			$('header .logo-branca').removeClass('show-logo-branca');
		});

		$('header .navbar .navbar-header .close-sub-menu').click(function(){
			$('header .navbar .navbar-header ul li .sub-menu').removeClass('show-sub-menu');
			$('header .navbar .navbar-header .close-sub-menu').removeClass('show-close-sub-menu');
		});

		if(screen.width <= 991){
			$('header .navbar .navbar-header ul li#menu-item-33>a').click(function(e){
				$('header .navbar .navbar-header ul li .sub-menu').addClass('show-sub-menu');
				setTimeout(function(){
					$('header .navbar .navbar-header .close-sub-menu').addClass('show-close-sub-menu');
				}, 200);
				return false;
			});
		}

		if(screen.width <= 500){
			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').addClass('col-6');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-8').addClass('col-6');

			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').removeClass('col-4');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-8').removeClass('col-8');

			$('.pg-projetos .menu-filtro .filtros .modelo img.grid').click(function(){
				$('.pg-projetos .secao-projetos .lista-projetos').addClass('grid-projetos');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').addClass('col-12');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').removeClass('col-6');
				$('.pg-projetos .secao-projetos .grid-projetos li:nth-child(3n+2)').addClass('evento-centro');
				$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
				$(this).addClass('modelo-active');
			});

			$('.pg-projetos .menu-filtro .filtros .modelo img.list').click(function(){
				$('.pg-projetos .secao-projetos .lista-projetos').removeClass('grid-projetos');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').addClass('col-6');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').removeClass('col-12');
				$('.pg-projetos .secao-projetos .grid-projetos li').removeClass('evento-centro');
				$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
				$(this).addClass('modelo-active');
			});
		}

		$('.pg-servicos section.servico-destaque nav.servico-destaque-menu .categoria-ativa a').click(function(){
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul').toggleClass('show-menu-categorias');
			return false;
		});

		$('.pop-up-agendamento .pop-up article .div-button-padrao .button-padrao').click(function(){
			$('.pop-up-agendamento .pop-up .formulario-contato').addClass('show-formulario');
		});

		$('.pop-up-agendamento .pop-up .close-sub-menu').click(function(){
			$('.pop-up-agendamento .pop-up .formulario-contato').removeClass('show-formulario');
		});

	});

}())